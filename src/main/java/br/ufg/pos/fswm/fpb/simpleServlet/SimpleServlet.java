package br.ufg.pos.fswm.fpb.simpleServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 27/05/17.
 */
@WebServlet(name = "simpleServlet", urlPatterns = {"/hello"})
public class SimpleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
            writer.println("<html>");
            writer.println("<head>");
            writer.println("<title>E ae</title>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<p>Olá mundo!</p>");
            writer.println("</body>");
            writer.println("</html>");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}
