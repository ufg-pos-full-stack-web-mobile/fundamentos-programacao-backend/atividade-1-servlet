package br.ufg.pos.fswm.fpb.simpleServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 27/05/17.
 */
@WebServlet(name = "FormServlet", urlPatterns = {"/form"})
public class FormServlet extends HttpServlet {

    public static final String UTF_8 = "UTF-8";
    public static final String TEXT_HTML = "text/html";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding(UTF_8);
        resp.setContentType(TEXT_HTML);
        resp.setCharacterEncoding(UTF_8);

        final String codigo = req.getParameter("codigo");
        final String nome = req.getParameter("nome");
        final String preco = req.getParameter("preco");

        Cookie cookieNome = new Cookie("nome", nome);
        cookieNome.setComment("Coockie que guarda o nome do produto");
        cookieNome.setMaxAge(24*60*60);
        cookieNome.setSecure(false);
        resp.addCookie(cookieNome);

        Cookie cookiePreco = new Cookie("preco", preco);
        cookiePreco.setComment("Coockie que guarda o preco do produto");
        cookiePreco.setMaxAge(24*60*60);
        cookiePreco.setSecure(false);
        resp.addCookie(cookiePreco);


        HttpSession session = req.getSession(true);
        session.setAttribute("codigo", codigo);
        System.out.println(session.getId()  );

        final String pgWeb = "<!DOCTYPE html><html>" +
                "<head><title>Produto Cadastrado</title></head>" +
                "<body>" +
                "<p>Produto cadastrado:</p>" +
                "<p>Nome: " + nome + "<br/>" +
                "Código: " + codigo + "<br/>" +
                "Preço: " + preco + "<br/>" +
                "</body>" +
                "</html>";

        resp.getWriter().write(pgWeb);
    }
}
